# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @classmethod
    def assign_try(cls, moves, with_childs=True, grouping=('product',)):
        to_assign_try = []
        for move in moves:
            if move.unit_load and move.shipment:
                continue
            to_assign_try.append(move)
        return super().assign_try(to_assign_try, with_childs=with_childs,
            grouping=grouping)
